package com.ramitsuri.whatsappundeleter;

import org.junit.Assert;
import org.junit.Test;

public class MyNotificationTest {
    @Test
    public void testIsWorthy() {
        String text;

        text = "1 new message";
        MyNotification notification = new MyNotification();
        notification.setText(text);
        Assert.assertFalse(notification.isWorthy());

        text = null;
        notification.setText(text);
        Assert.assertFalse(notification.isWorthy());

        text = "2 new message";
        notification.setText(text);
        Assert.assertFalse(notification.isWorthy());

        text = "3 new messages";
        notification.setText(text);
        Assert.assertFalse(notification.isWorthy());

        text = "300 new messages";
        notification.setText(text);
        Assert.assertFalse(notification.isWorthy());

        text = "";
        notification.setText(text);
        Assert.assertFalse(notification.isWorthy());

        text = "Hello world";
        notification.setText(text);
        Assert.assertTrue(notification.isWorthy());
    }
}
