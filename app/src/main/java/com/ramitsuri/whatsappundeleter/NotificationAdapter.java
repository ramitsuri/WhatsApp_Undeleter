package com.ramitsuri.whatsappundeleter;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class NotificationAdapter
        extends RecyclerView.Adapter<NotificationAdapter.CustomViewHolder> {
    private static String HIGHLIGHT = "delete";

    List<MyNotification> mNotifications;

    public NotificationAdapter(List<MyNotification> notifications) {
        mNotifications = notifications;
    }

    public void setNotifications(
            List<MyNotification> notifications) {
        mNotifications.clear();
        mNotifications.addAll(notifications);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,
            int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder,
            int position) {
        holder.mFieldDate
                .setText(AppHelper.getFormattedDate(mNotifications.get(position).getPostTime()));

        String title = mNotifications.get(position).getTitle();
        holder.mFieldTitle.setText(title);
        String text = mNotifications.get(position).getText();
        holder.mFieldText.setText(text);

        if (title.contains(HIGHLIGHT) || text.contains(HIGHLIGHT)) {
            holder.mContainer.setBackgroundColor(ContextCompat
                    .getColor(holder.mContainer.getContext(), android.R.color.holo_red_dark));
            holder.mFieldTitle.setTextColor(
                    ContextCompat.getColor(holder.mFieldTitle.getContext(), android.R.color.white));
            holder.mFieldText.setTextColor(
                    ContextCompat.getColor(holder.mFieldText.getContext(), android.R.color.white));
            holder.mFieldDate.setTextColor(
                    ContextCompat.getColor(holder.mFieldDate.getContext(), android.R.color.white));
        } else {
            holder.mContainer.setBackgroundColor(ContextCompat
                    .getColor(holder.mContainer.getContext(), android.R.color.white));
            holder.mFieldTitle.setTextColor(
                    ContextCompat.getColor(holder.mFieldTitle.getContext(), android.R.color.black));
            holder.mFieldText.setTextColor(
                    ContextCompat.getColor(holder.mFieldText.getContext(), android.R.color.black));
            holder.mFieldDate.setTextColor(
                    ContextCompat.getColor(holder.mFieldDate.getContext(), android.R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ViewGroup mContainer;
        private TextView mFieldDate, mFieldTitle, mFieldText;

        public CustomViewHolder(View itemView) {
            super(itemView);
            mContainer = itemView.findViewById(R.id.container);
            mFieldDate = itemView.findViewById(R.id.date);
            mFieldTitle = itemView.findViewById(R.id.title);
            mFieldText = itemView.findViewById(R.id.text);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
