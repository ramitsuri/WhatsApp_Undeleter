package com.ramitsuri.whatsappundeleter;

import android.app.ActivityManager;
import android.content.Context;
import android.service.notification.StatusBarNotification;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppHelper {
    private static String WHATSAPP_PACKAGE_NAME = "com.whatsapp";
    private static String GROUP_TO_IGNORE = "summary";
    private static String CHANNEL_GROUP = "group_chat_defaults_2";
    private static String CHANNEL_INDIVIDUAL = "individual_chat_defaults_1";
    private static String CHANNEL_SILENT = "silent_notifications_3";

    public static boolean isNotificationAccessEnabled(Context context) {
        boolean isNotificationAccessEnabled = false;
        ActivityManager manager =
                (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager
                    .getRunningServices(Integer.MAX_VALUE)) {
                if (NotificationListener.class.getName().equals(service.service.getClassName())) {
                    isNotificationAccessEnabled = true;
                }
            }
        }
        return isNotificationAccessEnabled;
    }

    public static boolean careAboutThisOne(StatusBarNotification sbn) {
        return sbn.getPackageName().equals(WHATSAPP_PACKAGE_NAME) &&
                (sbn.getNotification().getChannelId().equalsIgnoreCase(CHANNEL_GROUP) ||
                        sbn.getNotification().getChannelId().equalsIgnoreCase(CHANNEL_INDIVIDUAL) ||
                        sbn.getNotification().getChannelId().equalsIgnoreCase(CHANNEL_SILENT));
    }

    public static String getFormattedDate(long dateLong) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm", Locale.US);
        Date date = new Date(dateLong);
        return sdf.format(date);
    }
}
