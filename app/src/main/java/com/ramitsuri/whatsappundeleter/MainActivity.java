package com.ramitsuri.whatsappundeleter;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.ramitsuri.whatsappundeleter.db.MessagesHelper;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager recyclerViewLManager = new LinearLayoutManager(this);

        List<MyNotification> notifications = MessagesHelper.getNotifications(this);
        if (notifications.size() > 0) {
            findViewById(R.id.no_deleted).setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.no_deleted).setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        notificationAdapter = new NotificationAdapter(MessagesHelper.getNotifications(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(recyclerViewLManager);
        recyclerView.setAdapter(notificationAdapter);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessagesHelper.deleteAll(MainActivity.this);
                notificationAdapter
                        .setNotifications(MessagesHelper.getNotifications(MainActivity.this));
            }
        });

        getNotififcationChannel();
    }

    private void getNotififcationChannel() {
        NotificationManager mNotificationManager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.getNotificationChannels();
    }

    @Override
    protected void onResume() {
        super.onResume();
        notificationAdapter.setNotifications(MessagesHelper.getNotifications(this));
    }
}
