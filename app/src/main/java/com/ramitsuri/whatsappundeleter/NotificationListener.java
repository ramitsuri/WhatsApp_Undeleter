package com.ramitsuri.whatsappundeleter;

import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.ramitsuri.whatsappundeleter.db.MessagesHelper;

public class NotificationListener extends NotificationListenerService {

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        if (AppHelper.careAboutThisOne(sbn)) {
            MyNotification notification = new MyNotification(sbn);
            MessagesHelper.addNotification(this, notification);
        }
    }
}
