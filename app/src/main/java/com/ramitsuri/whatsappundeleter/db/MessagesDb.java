package com.ramitsuri.whatsappundeleter.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ramitsuri.whatsappundeleter.MyNotification;

import java.util.ArrayList;
import java.util.List;

import static com.ramitsuri.whatsappundeleter.db.DBConstants.TABLE_MESSAGES;

public class MessagesDb extends BaseDB {

    public MessagesDb(Context context) {
        super(context);
    }

    public String[] getAllColumns() {
        return new String[] {
                DBConstants.COLUMN_PACKAGE_NAME,
                DBConstants.COLUMN_POST_TIME,
                DBConstants.COLUMN_TITLE,
                DBConstants.COLUMN_TEXT
        };
    }

    public ContentValues getMessagesContentValues(MyNotification notification) {

        long dateTime = notification.getPostTime();
        String packageName = notification.getPackageName();
        String title = notification.getTitle();
        String text = notification.getText();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstants.COLUMN_POST_TIME, dateTime);
        contentValues.put(DBConstants.COLUMN_PACKAGE_NAME, packageName);
        contentValues.put(DBConstants.COLUMN_TITLE, title);
        contentValues.put(DBConstants.COLUMN_TEXT, text);

        return contentValues;
    }

    private MyNotification getNotificationFromCursor(Cursor cursor) {
        MyNotification notification = new MyNotification();
        for (String column : cursor.getColumnNames()) {
            if (column.equals(DBConstants.COLUMN_PACKAGE_NAME)) {
                String value = cursor.getString(
                        cursor.getColumnIndex(DBConstants.COLUMN_PACKAGE_NAME));
                notification.setPackageName(value);
            } else if (column.equals(DBConstants.COLUMN_POST_TIME)) {
                long value = cursor.getLong(
                        cursor.getColumnIndex(DBConstants.COLUMN_POST_TIME));
                notification.setPostTime(value);
            } else if (column.equals(DBConstants.COLUMN_TITLE)) {
                String value = cursor.getString(
                        cursor.getColumnIndex(DBConstants.COLUMN_TITLE));
                notification.setTitle(value);
            } else if (column.equals(DBConstants.COLUMN_TEXT)) {
                String value = cursor.getString(
                        cursor.getColumnIndex(DBConstants.COLUMN_TEXT));
                notification.setText(value);
            }
        }
        return notification;
    }

    public synchronized boolean setNotification(MyNotification notification) {
        open();
        boolean insertSuccess = true;
        ContentValues contentValues = getMessagesContentValues(notification);
        long result = mDatabase.insertOrThrow(TABLE_MESSAGES, null,
                contentValues);
        if (result <= 0) {
            insertSuccess = false;
        }
        close();
        return insertSuccess;
    }

    public List<MyNotification> getAllNotification() {
        open();

        String[] columns = getAllColumns();

        Cursor cursor = getCursor(TABLE_MESSAGES, columns, null, null, null, null, null, null);

        List<MyNotification> expenses = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    MyNotification expense = getNotificationFromCursor(cursor);
                    expenses.add(expense);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {

        }

        cursor.close();
        close();

        return expenses;
    }

    public void deleteAllExpense() {
        open();

        mDatabase.execSQL("delete from "+ DBConstants.TABLE_MESSAGES);
        close();
    }
}
