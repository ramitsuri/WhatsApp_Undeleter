package com.ramitsuri.whatsappundeleter.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class BaseDB {
    private SQLHelper mSQLHelper;
    protected SQLiteDatabase mDatabase;

    public BaseDB(Context context){
        mSQLHelper = SQLHelper.getInstance(context.getApplicationContext());
    }

    protected void open(){
        mDatabase = mSQLHelper.getWritableDatabase();
    }

    protected void close(){

    }

    public Cursor getCursor(String table, String[] columns, String selection,
            String[] selectionArgs, String groupBy, String having, String orderBy,
            String limit){
        open();
        return mDatabase.query(table, columns, selection, selectionArgs, groupBy, having, orderBy,
                limit);
    }

    public Cursor getCursor(String sql, String[] selectionArgs){
        open();
        return mDatabase.rawQuery(sql, selectionArgs);
    }

    protected boolean isTrue(int dbBoolean){
        return dbBoolean == 1;
    }

    protected String getCol(String table, String column){
        return table + "." + column;
    }

    protected String getCol(String table, String column, String as){
        return table + "." + column + " As " + as;
    }

    }
