package com.ramitsuri.whatsappundeleter.db;

public class DBConstants {

    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_MESSAGES = "messages";
    public static final String COLUMN_PACKAGE_NAME = "package_name";
    public static final String COLUMN_POST_TIME= "post_time";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_TEXT= "text";

    public static final String CREATE_TABLE_MESSAGES =
            "CREATE TABLE "
                    + TABLE_MESSAGES
                    + "("
                    + COLUMN_PACKAGE_NAME + " TEXT, "
                    + COLUMN_POST_TIME + " INTEGER, "
                    + COLUMN_TITLE+ " TEXT, "
                    + COLUMN_TEXT + " TEXT"
                    + ");"
            ;
}
