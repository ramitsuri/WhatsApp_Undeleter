package com.ramitsuri.whatsappundeleter.db;

import android.content.Context;

import com.ramitsuri.whatsappundeleter.MyNotification;

import java.util.List;

public class MessagesHelper {

    private static MessagesDb getDB(Context context){
        return new MessagesDb(context);
    }

    public static List<MyNotification> getNotifications(Context context){
        return getDB(context).getAllNotification();
    }

    public static boolean addNotification(Context context, MyNotification notification){
        if(notification.isWorthy()) {
            return getDB(context).setNotification(notification);
        }
        return false;
    }

    public static void deleteAll(Context context){
        getDB(context).deleteAllExpense();
    }

}
