package com.ramitsuri.whatsappundeleter;

import android.app.Notification;
import android.service.notification.StatusBarNotification;
import android.text.SpannableString;
import android.text.TextUtils;

public class MyNotification {

    private static String NEW_MESSAGES_PATTERN = "[0-9]+\\snew\\smessages*";
            // Will filter out <1 new message>, <3 new messages>
    private static String NEW_MULTIPLE_MESSAGES_PATTER =
            "[0-9]+\\smessage[s]*\\sfrom\\s[0-9]\\schat[s]*";
    private static String VOICE_CALLS_PATTERN = "Missed\\svoice\\scall";
    private static String VOICE_CALLS_MULTIPLE_PATTERN = "[0-9]+\\smissed\\scall[s]*";

    public static String ANDROID_TEXT = "android.text";
    public static String ANDROID_TITLE = "android.title";

    private String mPackageName;
    private long mPostTime;
    private String mTitle;
    private String mText;

    public MyNotification() {
    }

    public MyNotification(String packageName, long postTime, String title, String text) {
        mPackageName = packageName;
        mPostTime = postTime;
        mTitle = title;
        mText = text;
    }

    public MyNotification(StatusBarNotification sbn) {
        mPackageName = sbn.getPackageName();
        mPostTime = sbn.getPostTime();
        Notification notification = sbn.getNotification();
        mTitle = (String)notification.extras.get(ANDROID_TITLE);
        Object text = notification.extras.get(ANDROID_TEXT);
        if (text instanceof SpannableString) {
            mText = ((SpannableString)text).toString();
        } else if (text instanceof String) {
            mText = (String)text;
        }
    }

    public String getPackageName() {
        return mPackageName;
    }

    public void setPackageName(String packageName) {
        mPackageName = packageName;
    }

    public long getPostTime() {
        return mPostTime;
    }

    public void setPostTime(long postTime) {
        mPostTime = postTime;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public boolean isWorthy() {
        return !TextUtils.isEmpty(mText) &&
                !mText.matches(NEW_MESSAGES_PATTERN) &&
                !mText.matches(NEW_MULTIPLE_MESSAGES_PATTER) &&
                !mTitle.matches(VOICE_CALLS_PATTERN) &&
                !mTitle.matches(VOICE_CALLS_MULTIPLE_PATTERN);
    }
}
